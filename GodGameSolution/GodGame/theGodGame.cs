﻿using System;

namespace GodGame
{
    class theGodGame
    {
        public static void Afficher(GestionDeVie p_planete)
        {
            Console.WriteLine("Bonjour bienvenu dans le GODGame de qualité");
            Console.WriteLine("Voulez vous : créer, reproduire ou tuer");
            Console.WriteLine("Votre monde : nombre d'être vivant : {0}", p_planete.GetNumberOfLiving());
            Console.WriteLine("Liste d'être vivant et ce qu'il font : {0}", p_planete);
            Console.WriteLine("Que voulez faire ? Créer(1), Accoupler(2), Déplacer(3) ou tuer(4) ?");
        }

        static void Main(string[] args)
        {
            GestionDeVie m_laTerre = new GestionDeVie();
            while (true || Console.ReadLine().ToLower() == "exit")
            {
                String userChoice;
                bool goodChoice = false;
                Afficher(m_laTerre);
                userChoice = Console.ReadLine().ToLower();
                do
                {
                    switch (userChoice)
                    {
                        case "1":
                            m_laTerre.Create();
                            goodChoice = true;
                            break;
                        case "2":
                            m_laTerre.Reproduction();
                            goodChoice = true;
                            break;
                        case "3":
                            m_laTerre.Deplacer();
                            goodChoice = true;
                            break;
                        case "4":
                            m_laTerre.Kill();
                            goodChoice = true;
                            break;
                        default:
                            Console.WriteLine("Veuillez entrer une action valide");
                            goodChoice = true;
                            break;
                    }
                    m_laTerre.ToString();
                } while (goodChoice != true);

                //Console.WriteLine("Voici ce qui c'est passé : {0}", m_laTerre.WhatHappend());
            }
        }
    }
}
