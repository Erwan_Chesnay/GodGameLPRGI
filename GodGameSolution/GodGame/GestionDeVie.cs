﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GodGame.Regne;

namespace GodGame
{
    public class GestionDeVie
    {
        private List<EtreVivant> v_myCreature = new List<EtreVivant>();

        public GestionDeVie()
        {
        }

        public void Create()
        {
            Random m_randomNumber = new Random();
            int m_random = m_randomNumber.Next(1, 4);

            switch (m_random)
            {
                case 1:
                    Animal m_newAnimal = new Animal();
                    v_myCreature.Add(m_newAnimal);
                    break;
                case 2:
                    Bacterie m_newBacterie = new Bacterie();
                    v_myCreature.Add(m_newBacterie);
                    break;
                case 3:
                    Vegetaux m_newVegetal = new Vegetaux();
                    v_myCreature.Add(m_newVegetal);
                    break;
                default:
                    break;
            }
        }

        public void AddVegetal(Vegetaux p_newVegetal)
        {

        }

        public void AddAnimaux(Animal p_newAnimal)
        {

        }

        public void AddBacterie(Bacterie p_newBacterie)
        {

        }

        public int GetNumberOfLiving()
        {
            return v_myCreature.Count();
        }

        public void Deplacer()
        {
            int m_random = RandomNumberCreator();
            for (int i = 0; i < m_random; i++)
            {
                EtreVivant m_livingCreature = RandomSelector();
                m_livingCreature.Deplacer();
            }

        }

        public void Kill()
        {
            int m_random = RandomNumberCreator();
            for (int i = 0; i < m_random; i++)
            {
                EtreVivant m_livingCreature = RandomSelector();
                v_myCreature.Remove(m_livingCreature);
            }
        }

        public void Reproduction()
        {
            EtreVivant m_livingCreatureParent1 = RandomSelector();
            EtreVivant m_livingCreatureParent2 = RandomSelector(m_livingCreatureParent1.GetType(), m_livingCreatureParent1.GetName());
            if(m_livingCreatureParent1.GetType() == m_livingCreatureParent2.GetType()){
                EtreVivant m_enfant = m_livingCreatureParent1.Reproduction();
                v_myCreature.Add(m_enfant);
            } else {
                Console.WriteLine("Pas assez de créature pour accoupler");
            }
        }

        public EtreVivant RandomSelector(Type p_type = null, String p_parentName = "")
        {
            EtreVivant m_myLivingCreature = null;
            m_myLivingCreature = v_myCreature.ElementAt(RandomNumberCreator());
            if (null != p_type)
            {
                do
                {
                    m_myLivingCreature = RandomSelector();
                } while (m_myLivingCreature.GetType() != p_type && m_myLivingCreature.GetName() != p_parentName);
                /*IEnumerable<EtreVivant> m_myAnimalList = v_myCreature.Where(t => t is p_type);
                m_livingIndex = m_randomNumber.Next(0, m_myAnimalList.Count()-1);
                m_myLivingCreature = m_myAnimalList.ElementAt(m_livingIndex);*/
            }
            return m_myLivingCreature;
        }

        public int RandomNumberCreator(){
            Random m_randomNumber = new Random();
            int m_livingIndex = m_randomNumber.Next(0, v_myCreature.Count() - 1);
            return m_livingIndex;
        }

        public override string ToString()
        {
            String m_affichage = "Situation du monde : " + Environment.NewLine;
            foreach(EtreVivant m_etreVivant in v_myCreature){
                m_affichage += "    Nom = " + m_etreVivant.GetName() + Environment.NewLine;
                m_affichage += "    Race = " + m_etreVivant.GetRaceName() + Environment.NewLine;
                m_affichage += "    Action = " + m_etreVivant.GetAction() + Environment.NewLine;
                m_affichage += "-------------------" + Environment.NewLine;
            }
            return m_affichage;
        }
    }
}
