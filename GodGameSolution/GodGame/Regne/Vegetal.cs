﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GodGame.Regne
{
    public class Vegetaux : EtreVivant
    {

        List<string> v_vegetalName = new List<string> {
            "Tulipe",
            "Rose",
            "Chêne",
            "Thuya",
        };

        List<string> v_vegetalRace = new List<string> {
            "Fleur",
            "Arbre",
            "Buisson"
        };

        public Vegetaux()
        {
            Init();
            v_actionEnCours = "de ne rien faire";
        }

        public override EtreVivant Reproduction()
        {
            Vegetaux enfant = new Vegetaux();
            enfant.Init(v_race);

            return enfant;
        }

        public override void Init(String m_parentRace = "")
        {
            Random m_randomNumber = new Random();

            if ("" == m_parentRace)
            {
                int m_raceNumber = m_randomNumber.Next(0, v_vegetalRace.Count());
                v_race = v_vegetalRace.ElementAt(m_raceNumber);
            }
            else
            {
                v_race = m_parentRace;
            }

            int m_name = m_randomNumber.Next(0, v_vegetalName.Count());
            v_name = v_vegetalName.ElementAt(m_name);

        }

        public override void Deplacer()
        {
            v_actionEnCours = "étendre";
        }
    }
}
