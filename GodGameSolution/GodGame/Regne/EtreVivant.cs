﻿using System;
namespace GodGame.Regne
{
    public abstract class EtreVivant
    {
        protected String v_name;
        protected String v_race;
        protected String v_actionEnCours;



        public string GetName()
        {
            return v_name;
        }

        public string GetRaceName()
        {
            return v_race;
        }

        public string GetAction(){
            return v_actionEnCours;
        }

        public abstract EtreVivant Reproduction();

        public abstract void Deplacer();

        public abstract void Init(String m_parentRace = "");

        public static implicit operator EtreVivant(Type v)
        {
            throw new NotImplementedException();
        }
    }
}
