﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GodGame.Regne
{
    public class Bacterie : EtreVivant
    {

        List<string> v_bacterieName = new List<string> {
            "Pascal",
            "PasBien",
            "Killer",
            "Despacito",
        };

        List<string> v_bacterieRace = new List<string> {
            "Afipia felis",
            "Helicobacter pyroli",
            "Escherichia coli",
        };

        public Bacterie()
        {
            Init();
            v_actionEnCours = "de ne rien faire";
        }

        public override EtreVivant Reproduction()
        {
            Bacterie enfant = new Bacterie();
            enfant.Init(v_race);

            return enfant;
        }

        public override void Init(String m_parentRace = "")
        {
            Random m_randomNumber = new Random();

            if ("" == m_parentRace)
            {
                int m_raceNumber = m_randomNumber.Next(0, v_bacterieRace.Count());
                v_race = v_bacterieRace.ElementAt(m_raceNumber);
            }
            else
            {
                v_race = m_parentRace;
            }

            int m_nameBacterie = m_randomNumber.Next(0, v_bacterieName.Count());
            v_name = v_bacterieName.ElementAt(m_nameBacterie);
           
        }

        public override void Deplacer()
        {
            v_actionEnCours = "ramper";
        }
    }
}
