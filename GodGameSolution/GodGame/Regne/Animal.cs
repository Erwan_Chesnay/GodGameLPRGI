﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GodGame.Regne
{
    public class Animal : EtreVivant
    {
        private String v_sexe;

        List<string> v_animalNameMale = new List<string> { 
            "Jean", 
            "Pierre", 
            "Fred", 
            "Joe",
        };

        List<string> v_animalNameFemale = new List<string> {
            "Marie",
            "Jeanne",
            "Josseline",
            "Brigitte",
        };

        List<string> v_animalRace = new List<string> {
            "Homme",
            "Serpent",
            "Poisson",
            "Oiseau",
        };

        public Animal()
        {
            Init();
            v_actionEnCours = "de ne rien faire";
        }

        public Animal(String p_parentRace)
        {
            Init(p_parentRace);
            v_actionEnCours = "de ne rien faire";
        }

        public override void Deplacer()
        {
            switch (v_race)
            {
                case "Homme":
                    v_actionEnCours = "marcher";
                    break;
                case "Chat":
                    v_actionEnCours = "courir";
                    break;
                case "Poisson":
                    v_actionEnCours = "nager";
                    break;
                case "Oiseau":
                    v_actionEnCours = "voler";
                    break;
                default:
                    v_actionEnCours = "de ne rien faire";
                    break;
            }
        }

        public override EtreVivant Reproduction() {
            Animal enfant = new Animal(v_race);
            return enfant;
        }

        public override void Init(String m_parentRace = ""){
            Random m_randomNumber = new Random();
            int m_sexe = m_randomNumber.Next(1, 3);
            v_sexe = "une femelle";
            if (1 == m_sexe)
            {
                v_sexe = "un mâle";
            }

            if ("" == m_parentRace)
            {
                int m_raceNumber = m_randomNumber.Next(0, v_animalRace.Count());
                v_race = v_animalRace.ElementAt(m_raceNumber);
            } else {
                v_race = m_parentRace;
            }

            if ("un mâle" == v_sexe)
            {
                int m_nameMale = m_randomNumber.Next(0, v_animalNameMale.Count());
                v_name = v_animalNameMale.ElementAt(m_nameMale);
            } else {
                int m_nameFemale = m_randomNumber.Next(0, v_animalNameFemale.Count());
                v_name = v_animalNameFemale.ElementAt(m_nameFemale);
            }
        }
    }
}